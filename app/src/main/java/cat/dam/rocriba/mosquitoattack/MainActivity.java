package cat.dam.rocriba.mosquitoattack;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    //Inicalize variables
  private  ImageView iv_mosquit;
    private AnimationDrawable mosquit_animat;
    TextView contador;
    int varcomptador = 0;
    final Handler handler = new Handler();
    final Handler handler2 = new Handler();
    final Runnable r = new Runnable() {
        long l = (long) genereateRandommiliseconds();
        public void run() {
            handler.postDelayed(this, l);
            generarMosquit();
        }
    };
    final Runnable r2 = new Runnable() {
        public void run() {
            handler2.postDelayed(this, 1500);
            handler.removeCallbacks(r);
            endGame();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Start Postdelayed and call function generarMosquit(),
        //starting the animation
        handler2.postDelayed(r2, 15000);
        generarMosquit();

    }

    @Override
    protected void onStart(){
        super.onStart();
        mosquit_animat = (AnimationDrawable) iv_mosquit.getBackground();
        mosquit_animat.start();
    }
    //Genereates a random float
    protected  float genereateRandom(){
        int min = 1;
        int max = 1000;
        Random r = new Random();
        float random = min + r.nextFloat() * (max - min);
        return random;

    }
    //Generate a random float
    protected  float genereateRandommiliseconds(){
        int min = 500;
        int max = 6000;
        Random r = new Random();
        float random = min + r.nextFloat() * (max - min);
        return random;

    }
    //Every time is called, enerates an animation mosquito in a random place in the screen
    //if the mosquito is touched, starts the other animations (blood animation)
    protected void generarMosquit(){
        mosquit_animat = new AnimationDrawable();

        iv_mosquit = (ImageView) findViewById(R.id.iv_animada);
        contador = findViewById(R.id.tv_counter);

        iv_mosquit.setX(genereateRandom());
        iv_mosquit.setY(genereateRandom());
        iv_mosquit.setBackgroundResource(R.drawable.mosquit_animat);
        mosquit_animat = (AnimationDrawable) iv_mosquit.getBackground();
        mosquit_animat.start();
        iv_mosquit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iv_mosquit.setBackgroundResource(R.drawable.sang_animat);
                mosquit_animat = (AnimationDrawable) iv_mosquit.getBackground();
                mosquit_animat.start();
                varcomptador++;
                handler.postDelayed(r, 1500);
            }
        });
    }
    //After 15 seconds we call endGame()function
    //Puts the button and texts(counter) visible and stop the game
    //Option restart or get out  of the application have to be selected, if restart, we
    //Restart the handler of 15 seconds and everyting
    protected  void endGame(){
        Button reiniciar = (Button) findViewById(R.id.btn_reiniciar);
        Button sortir = (Button) findViewById(R.id.btn_sortir);

        contador.setVisibility(View.VISIBLE);
        reiniciar.setVisibility(View.VISIBLE);
        sortir.setVisibility(View.VISIBLE);

        contador.setText("ENCERTS: " + varcomptador);

        reiniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // handler.postDelayed(r, 1500);
                contador.setVisibility(View.INVISIBLE);
                reiniciar.setVisibility(View.INVISIBLE);
                sortir.setVisibility(View.INVISIBLE);
                generarMosquit();
                handler2.removeCallbacks(r2);
                varcomptador = 0;
                handler2.postDelayed(r2, 15000);

            }
        });
        sortir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAndRemoveTask();
            }
        });
    }
}